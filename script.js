// var num = 4;
    function addItem(){

        var cand = $('#candidate :selected').val();
        if(cand == 0){
            alert("error");
            return false
        }
        var num = ($('#num').val())*1;

        num+=1;
        $('#num').val(num);

        var ul = document.getElementById("dynamic-list");
        var table_th = document.getElementById("customers_th");
        var table_td = document.getElementById("customers_td");
        var candidate = document.getElementById("candidate");
        var candidate = document.getElementById("candidate");
        var step = $("#input-step").val();
        var curcolor = $('#candidate :selected').val();
        var curid = $('#candidate :selected').attr("id");
        var curname = $('#candidate :selected').text();
        var input1 = document.createElement("input");
        var input2 = document.createElement("input");
        var label = document.createElement("label");
        var div = document.createElement("div");
        var th = document.createElement("th");
        var td = document.createElement("td");
        var span1 = document.createElement("span");
        var span2 = document.createElement("span");
        var span3 = document.createElement("span");
        var span4 = document.createElement("span");
        var button = document.createElement("button");
        var br = document.createElement("br");
        span1.setAttribute('id','span-'+num);
        span2.setAttribute('id','label-'+num);
        span2.setAttribute('class','span');
        span3.setAttribute('class','span');
        span4.setAttribute('class','span-br');
        div.setAttribute('id','id-'+num);
        th.setAttribute('id','th-'+num);
        td.setAttribute('id','td-'+num);
        input1.setAttribute('id','input-'+num);
        input1.setAttribute('name', 'input-'+num+'_id-'+curid+'-price-'+curcolor);
        input1.setAttribute('value', 0);
        input1.setAttribute('type', 'range');
        input1.setAttribute('style', ' width:100%;');
        input1.setAttribute('oninput','calc(this.value, '+num+');');
        input1.setAttribute('step', step);
        button.setAttribute('class','rem-item');
        button.setAttribute('onclick','removeItem(this)');
        button.setAttribute('data-id',num);

        input2.setAttribute('id','ml_price-'+num);
        input2.setAttribute('value', curcolor);
        input2.setAttribute('type', 'hidden');

        label.setAttribute('for','input-'+num);
        span2.appendChild(document.createTextNode(0));
        span3.appendChild(document.createTextNode('ml (' + curcolor + 'р за 1ml):'));
        span4.appendChild(document.createTextNode(curname));
        th.appendChild(document.createTextNode(num));
        td.appendChild(document.createTextNode(0));
        span1.appendChild(document.createTextNode(0));
        button.appendChild(document.createTextNode('remove'));

        ul.appendChild(div);
        div.appendChild(label);
        label.appendChild(span4);
        label.appendChild(br);
        label.appendChild(span2);
        label.appendChild(span3);
        div.appendChild(input1);
        div.appendChild(input2);
        div.appendChild(span1);
        div.appendChild(document.createTextNode(' %'));
        div.appendChild(button);

        table_th.appendChild(th);
        table_td.appendChild(td);

    }


function removeItem(element){

    var num = ($('#num').val())*1;

    if(num <= 4){
        alert("error");
        return false
    }

    var rem_num = $(element).data('id');
    var ul = document.getElementById("dynamic-list");
    var table_th = document.getElementById("customers_th");
    var table_td = document.getElementById("customers_td");
    var candidate = document.getElementById("candidate");
    var item = document.getElementById('id-'+rem_num);
    var item_th = document.getElementById('th-'+rem_num);
    var item_td = document.getElementById('td-'+rem_num);
    ul.removeChild(item);
    table_th.removeChild(item_th);
    table_td.removeChild(item_td);

}

function calc(par, number) {
    var count = $('#num').val();
    var ml_price = $('#ml_price-'+number).val();
    var type = ($('#type').val())*1;
    var par_val = (ml_price*(type*(par/100)));
    par_val =  Number((par_val).toFixed(3));
    var num = ($('#num').val())*1;

    document.getElementById("td-"+number).innerHTML = par_val;
    document.getElementById("span-"+number).innerHTML = (par);
if(number != 0){
    $('#label-'+number).text((type*par)/100);
}
    if(number == 0){

        // var params = ($('#type-' + par).val())*1;

        if(par == 30) {
            params = 75;
        }else if(par == 50){
            params = 100;
        }else if(par == 100){
            params = 200;
        }
        document.getElementById("td-0").innerHTML = params;
        document.getElementById("span-0").innerHTML = params;
        for (k = 1; k <= count; k++) {
            if (typeof($('#input-' + k).val()) !== 'undefined') {
                var val = $('#input-'+k).val();
                var ml_price = $('#ml_price-'+k).val();
                $('#label-'+k).text((type*val)/100);
                $('#td-'+k).text(ml_price*((type*val)/100));
            }

        }

    }

    if(number == 1){
        var val_all = 0;
        for (k = 3; k <= count; k++) {

            if ($('#input-' + k).val()) {
                var value = ($('#input-' + k).val()) * 1;
                    val_all = val_all + value;
            }else{
                value == 0;
            }
        }
        var val_all_1 = val_all + ((par)*1);

        var val = 100 - val_all_1;
        if(val < 0){
            val = 0;
            $('#input-1').val(100-val_all);
            var ml_price = $('#ml_price-1').val();
            document.getElementById("span-1").innerHTML = (100-val_all);
            document.getElementById("td-1").innerHTML = (ml_price*(type*((100-val_all)/100)));
            document.getElementById("label-1").innerHTML = ((type*(100-val_all))/100);
        }


        $('#input-2').val(val);
        document.getElementById("span-2").innerHTML = val;
        var ml_price_2 = $('#ml_price-2').val();
        var par_val_2 = (ml_price_2*(type*(val/100)));
        par_val_2 =  Number((par_val_2).toFixed(3));
        document.getElementById("td-2").innerHTML = par_val_2;
        document.getElementById("label-2").innerHTML = ((type*val)/100);

    }else if(number != 0){

        var val_all = 0;
        for (k = 2; k <= count; k++) {
            if (typeof($('#input-' + k).val()) !== 'undefined') {
                var value = ($('#input-' + k).val()) / 1;
                val_all = val_all + value;
            }
        }
        var val = 100 - val_all;

        if(val < 0){
            val = 0;
            var un_val = 100 - (val_all-par);
            var ml_price = $('#ml_price-'+number).val();
            $('#input-'+number).val(un_val);
            un_val = Number((un_val).toFixed(3));
            document.getElementById("span-"+number).innerHTML = (un_val);
            var td_val = ml_price*(type*((un_val)/100))
            var label_val = (type*un_val)/100
            td_val = Number((td_val).toFixed(3));
            label_val = Number((label_val).toFixed(3));
            document.getElementById("td-"+number).innerHTML = (td_val);
            document.getElementById("label-"+number).innerHTML = (label_val);

        }

        val = Number((val).toFixed(3));
        $('#input-1').val(val);
        document.getElementById("span-1").innerHTML = val;
        var ml_price = $('#ml_price-1').val();
        var par_val_1 = (ml_price*(type*(val/100)));
        par_val_1 =  Number((par_val_1).toFixed(3));
        var label_val = (type*val)/100
        label_val = Number((label_val).toFixed(3));
        document.getElementById("td-1").innerHTML = par_val_1;
        document.getElementById("label-1").innerHTML = (label_val);

    }

    // var params = ($('#type-' + par).val())*1;

    if(type == 30) {
        params = 75;
    }else if(type == 50){
        params = 100;
    }else if(type == 100){
        params = 200;
    }


    var sum = params;

    if(number == 0) {
        // console.log(type);
    }
    for (i = 1; i <= num; i++) {
        if (typeof($('#td-'+i)) !== 'undefined') {
            // console.log($('#td-'+i))
            sum = sum + ($('#td-'+i).text())*1;
        }
    }

    $('#customers_summ').val(sum);


}

